/**
  Copyright (c) 2019 "HEIG-VD, ReDS Institute"
  [http://reds.heig-vd.ch]

  Authors: Sydney HAUKE <sydney.hauke@heig-vd.ch>
           Kevin JOLY <kevin.joly@heig-vd.ch>

  This file is part of LDPC_C_Simulator.

  LDPC_C_Simulator is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "CDecoder_MS_fixed_flooded.h"

const size_t CDecoder_MS_fixed_flooded::nb_vn = _N;
const char CDecoder_MS_fixed_flooded::hard_decision_threshold = 0;

CDecoder_MS_fixed_flooded::CDecoder_MS_fixed_flooded()
{
    const size_t nb_edges = _M;
    const size_t nb_rows = _K;
    const size_t nb_cols = _N;
    size_t row;
    size_t vn_slot_offset, cn_slot_offset;
    size_t vn_idx;

    // We allocate a max of cn_deg_max per CN
    p_vn_addr = new char *[nb_rows * cn_deg_max];
    size_t* p_vn_addrSize = new size_t[nb_rows];

    for(size_t i = 0; i < nb_rows; i++) {
        p_vn_addrSize[i] = 0;
    }

    // We allocate a max of vn_deg_max per VN
    p_cn_addr = new char *[nb_cols * vn_deg_max];
    p_cn_addrSize = new size_t[nb_cols];

    for(size_t i = 0; i < nb_cols; i++) {
        p_cn_addrSize[i] = 0;
    }

    /* Iterate over the entire matrix H */
    for(size_t edge_idx = 0; edge_idx < nb_edges; edge_idx++) {
        vn_idx = PosNoeudsVariable[edge_idx];

        /* The following line is quite problematic. We assume that all CNs
         * have the same degree like it is the case with a DVB-S2 LDPC code.
         * (excluding the last CN, but it's not a problem here).
         *
         * What if the code has variable degree per CN ?
         */
        row = edge_idx / cn_deg_max; // Also indicates the current CN

        // Associate vn addr to cn index, and cn addr to vn index
        vn_slot_offset = p_vn_addrSize[row];
        cn_slot_offset = p_cn_addrSize[vn_idx];

        if (p_vn_addrSize[row] < cn_deg_max) {
            p_vn_addrSize[row]++;
        } else {
            std::cerr << "vn_addr offset out of bound" << std::endl;
            break;
        }

        if (p_cn_addrSize[vn_idx] < vn_deg_max) {
            p_cn_addrSize[vn_idx]++;
        } else {
            std::cerr << "cn_addr offset out of bound" << std::endl;
            break;
        }

        p_vn_addr[row*cn_deg_max + vn_slot_offset] =
            &_vn_msgs[vn_idx*vn_deg_max + cn_slot_offset];
        p_cn_addr[vn_idx*vn_deg_max + cn_slot_offset] =
            &_cn_msgs[row*cn_deg_max + vn_slot_offset];
    }

    delete[] p_vn_addrSize;
}

CDecoder_MS_fixed_flooded::~CDecoder_MS_fixed_flooded()
{
    delete p_vn_addr;
    delete p_cn_addr;

    delete p_cn_addrSize;
}

void CDecoder_MS_fixed_flooded::decode(
    char var_nodes[],
    char Rprime_fix[],
    int nombre_iterations)
{

    /* Initialize variable node messages */
    for(size_t i = 0; i < nb_vn; i++) {
        for(size_t j = 0; j < vn_deg_max; j++) {
            _vn_msgs[i*vn_deg_max + j] = var_nodes[i];
        }
    }

    /* Loop a fixed number of iterations */
    while(nombre_iterations--) {

        /* Check node updates.
         * For each check node, send a message containing a new estimate
         * to each of its neighbouring variable node.
         */
        for(size_t cn_idx = 0; cn_idx < DEG_1_COMPUTATIONS; cn_idx++) {

            int cn_offset = cn_idx*cn_deg_max;

            char signGlobal = 0;
            char minLLR1 = vSAT_POS_MSG;
            char minLLR2 = vSAT_POS_MSG;

            char abs[DEG_1];

            /* We compute a different estimate (the message sent), for each
             * neighbouring VN */
            for(size_t edge_idx = 0; edge_idx < DEG_1; edge_idx++) {
                char vn = *p_vn_addr[cn_offset + edge_idx];
                signGlobal ^= vn;
                abs[edge_idx] = (vn >= 0 ? vn : -vn);
                minLLR2 = minLLR2 < abs[edge_idx] ? minLLR2 : abs[edge_idx] > minLLR1 ? abs[edge_idx] : minLLR1;
                minLLR1 = minLLR1 < abs[edge_idx] ? minLLR1 : abs[edge_idx];
            }

#if (DEG_1 & 0x1)
            signGlobal = ~signGlobal;
#endif

            for(size_t edge_idx = 0; edge_idx < DEG_1; edge_idx++) {
                char sign = *p_vn_addr[cn_offset + edge_idx] ^ signGlobal;
                char min = abs[edge_idx] == minLLR1 ? minLLR2 : minLLR1;
                _cn_msgs[cn_offset + edge_idx] = sign < 0 ? -min : min;
            }
        }

#if (NB_DEGRES <= 2)
        for(size_t cn_idx = DEG_1_COMPUTATIONS;
            cn_idx < DEG_1_COMPUTATIONS + DEG_2_COMPUTATIONS;
            cn_idx++) {

            int cn_offset = cn_idx*cn_deg_max;

            char signGlobal = 0;
            char minLLR1 = vSAT_POS_MSG;
            char minLLR2 = vSAT_POS_MSG;

            char abs[DEG_2];

            /* We compute a different estimate (the message sent), for each
             * neighbouring VN */
            for(size_t edge_idx = 0; edge_idx < DEG_2; edge_idx++) {
                char vn = *p_vn_addr[cn_offset + edge_idx];
                signGlobal ^= vn;
                abs[edge_idx] = (vn >= 0 ? vn : -vn);
                minLLR2 = minLLR2 < abs[edge_idx] ? minLLR2 : abs[edge_idx] > minLLR1 ? abs[edge_idx] : minLLR1;
                minLLR1 = minLLR1 < abs[edge_idx] ? minLLR1 : abs[edge_idx];
            }

#if (DEG_2 & 0x1)
            signGlobal = ~signGlobal;
#endif

            for(size_t edge_idx = 0; edge_idx < DEG_2; edge_idx++) {
                char sign = *p_vn_addr[cn_offset + edge_idx] ^ signGlobal;
                char min = abs[edge_idx] == minLLR1 ? minLLR2 : minLLR1;
                _cn_msgs[cn_offset + edge_idx] = sign < 0 ? -min : min;
            }
        }
#else
#error "Code should be adapted to handle more degrees"
#endif
        /* Bit node updates.
         * For each variable node, compute two things :
         * 1) A new message to be sent to each of its neighbouring CNs
         * 2) A new estimate to be kept to itself
         */
        for(size_t vn_idx = 0; vn_idx < nb_vn; vn_idx++) {
            int estimate_acc = var_nodes[vn_idx];
            size_t vn_offset = vn_idx*vn_deg_max;

            for(size_t edge_idx = 0; edge_idx < p_cn_addrSize[vn_idx]; edge_idx++)
            {
                estimate_acc += *p_cn_addr[vn_offset + edge_idx];
            }

            if(estimate_acc > vSAT_POS_VAR) _vn_est[vn_idx] = vSAT_POS_VAR;
            else if(estimate_acc < vSAT_NEG_VAR) _vn_est[vn_idx] = vSAT_NEG_VAR;
            else _vn_est[vn_idx] = (char)estimate_acc;

            for(size_t edge_idx = 0; edge_idx < p_cn_addrSize[vn_idx]; edge_idx++)
            {
                int msg = *p_cn_addr[vn_offset + edge_idx];
                int vn_msg = estimate_acc - msg;

                if(vn_msg > vSAT_POS_VAR) vn_msg = vSAT_POS_VAR;
                else if(vn_msg < vSAT_NEG_VAR) vn_msg = vSAT_NEG_VAR;

                _vn_msgs[vn_offset + edge_idx] = (char)vn_msg;
            }
        }
    }

    /* Hard decision */
    for(size_t vn_idx = 0; vn_idx < nb_vn; vn_idx++) {
        Rprime_fix[vn_idx] = _vn_est[vn_idx] > hard_decision_threshold ? 1 : 0;
    }
}

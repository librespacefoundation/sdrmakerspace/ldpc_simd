#ifndef __CDecoder_layered_
#define __CDecoder_layered_

#include "../../Constantes/constantes_sse.h"

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <stdint.h>

#include "./CDecoder_fixed.h"

class CDecoder_fixed_layered : public CDecoder_fixed {
protected:
    const size_t cn_deg_max = DEG_1;
    //char *c_to_v_msgs;
    //char *var_nodes; 
    int16_t *c_to_v_msgs;
    
public:
    CDecoder_fixed_layered();
    virtual ~CDecoder_fixed_layered();
    virtual void decode(char  var_nodes[], char Rprime_fix[], int nombre_iterations) = 0;
    virtual void decode(float var_nodes[], char Rprime_fix[], int nombre_iterations);
};

#endif

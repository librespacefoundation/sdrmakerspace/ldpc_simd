
#include "./CDecoder_fixed_flooded.h"

#include "./Constantes/constantes_sse.h"

const size_t CDecoder_fixed_flooded::cn_deg_max = DEG_1;
const size_t CDecoder_fixed_flooded::vn_deg_max = DEG_VN_MAX;

CDecoder_fixed_flooded::CDecoder_fixed_flooded()
{
    _vn_est = new char[NOEUD];
    _vn_msgs   = new char[vn_deg_max * _N];
    _cn_msgs = new char[cn_deg_max * _K];
}

CDecoder_fixed_flooded::~CDecoder_fixed_flooded()
{
    delete _vn_est;
    delete _vn_msgs;
    delete _cn_msgs;
}

void CDecoder_fixed_flooded::decode(float var_nodes[], char Rprime_fix[], int nombre_iterations)
{
}

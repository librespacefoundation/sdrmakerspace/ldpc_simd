add_executable(flooded-decoder-test flooded-decoder-test.c
                                    ${PROJECT_SOURCE_DIR}/test/helpers.c)

target_link_libraries(flooded-decoder-test PUBLIC ldpc-dvbs2-decoder ${CMOCKA_LIBRARIES})

target_include_directories(flooded-decoder-test PRIVATE
    ${PROJECT_SOURCE_DIR}/test
)

add_test(NAME flooded-decoder-test
         COMMAND flooded-decoder-test)

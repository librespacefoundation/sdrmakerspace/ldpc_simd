/**
  Copyright (c) 2019 "HEIG-VD, ReDS Institute"
  [http://reds.heig-vd.ch]

  Authors: Sydney HAUKE <sydney.hauke@heig-vd.ch>
           Kevin JOLY <kevin.joly@heig-vd.ch>

  This file is part of ldpc-dvbs2-decoder.

  ldpc-dvbs2-decoder is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>

#include "ldpc-dvbs2-decoder/ldpc-dvbs2-decoder.h"

#include "decoders/flooded/flooded-decoder-64800x32400-dvb-s2.h"
#include "decoders/flooded/flooded-decoder-64800x6480-dvb-s2.h"
#include "decoders/flooded/flooded-decoder-64800x7200-dvb-s2.h"

#include "decoders/layered/layered-decoder-64800x32400-dvb-s2.h"
#include "decoders/layered/layered-decoder-64800x6480-dvb-s2.h"
#include "decoders/layered/layered-decoder-64800x7200-dvb-s2.h"

LDPC_DVBS2_DECODER_Error_t LDPC_DVBS2_DECODER_Init(LDPC_DVBS2_DECODER_Decoder_t *decoder,
                          LDPC_DVBS2_DECODER_Scheduler_t scheduler,
                          LDPC_DVBS2_DECODER_Code_t code)
{
    LDPC_DVBS2_DECODER_Error_t ret = LDPC_DVBS2_DECODER_ERROR_NONE;

    decoder->scheduler = scheduler;
    decoder->code = code;

    switch(decoder->scheduler) {
        case LDPC_DVBS2_DECODER_SCHEDULER_FLOODED:
            switch (code) {
                case LDPC_DVBS2_DECODER_CODE_DVB_S2_64800_32400:
                    ret = LDPC_DVBS2_DECODER_flooded_64800x32400_dvb_s2_Init(decoder);
                    break;
                case LDPC_DVBS2_DECODER_CODE_DVB_S2_64800_6480:
                    ret = LDPC_DVBS2_DECODER_flooded_64800x6480_dvb_s2_Init(decoder);
                    break;
                case LDPC_DVBS2_DECODER_CODE_DVB_S2_64800_7200:
                    ret = LDPC_DVBS2_DECODER_flooded_64800x7200_dvb_s2_Init(decoder);
                    break;
                default:
                    ret = LDPC_DVBS2_DECODER_ERROR_WRONG_PARAMETER;
                    fprintf(stderr, "Unkown code argument: %d\n", code);
                    break;
            }
            break;
     case LDPC_DVBS2_DECODER_SCHEDULER_LAYERED:
            switch (code) {
                case LDPC_DVBS2_DECODER_CODE_DVB_S2_64800_32400:
                    ret = LDPC_DVBS2_DECODER_layered_64800x32400_dvb_s2_Init(decoder);
                    break;
                case LDPC_DVBS2_DECODER_CODE_DVB_S2_64800_6480:
                    ret = LDPC_DVBS2_DECODER_layered_64800x6480_dvb_s2_Init(decoder);
                    break;
                case LDPC_DVBS2_DECODER_CODE_DVB_S2_64800_7200:
                    ret = LDPC_DVBS2_DECODER_layered_64800x7200_dvb_s2_Init(decoder);
                    break;
                default:
                    ret = LDPC_DVBS2_DECODER_ERROR_WRONG_PARAMETER;
                    fprintf(stderr, "Unkown code argument: %d\n", code);
                    break;
            }
            break;
        default:
            ret = LDPC_DVBS2_DECODER_ERROR_WRONG_PARAMETER;
            fprintf(stderr, "Unkown scheduler argument: %d\n", code);
            break;
    }

    return ret;
}

LDPC_DVBS2_DECODER_Error_t LDPC_DVBS2_DECODER_Terminate(LDPC_DVBS2_DECODER_Decoder_t *decoder)
{
    LDPC_DVBS2_DECODER_Error_t ret = LDPC_DVBS2_DECODER_ERROR_NONE;

    switch(decoder->scheduler) {
        case LDPC_DVBS2_DECODER_SCHEDULER_FLOODED:
            switch (decoder->code) {
                case LDPC_DVBS2_DECODER_CODE_DVB_S2_64800_32400:
                    LDPC_DVBS2_DECODER_flooded_64800x32400_dvb_s2_Terminate(decoder);
                    break;
                case LDPC_DVBS2_DECODER_CODE_DVB_S2_64800_6480:
                    LDPC_DVBS2_DECODER_flooded_64800x6480_dvb_s2_Terminate(decoder);
                    break;
                case LDPC_DVBS2_DECODER_CODE_DVB_S2_64800_7200:
                    LDPC_DVBS2_DECODER_flooded_64800x7200_dvb_s2_Terminate(decoder);
                    break;
                default:
                    ret = LDPC_DVBS2_DECODER_ERROR_RUNTIME;
                    fprintf(stderr, "Unkown code: %d\n", decoder->code);
                    break;
            }
            break;
        case LDPC_DVBS2_DECODER_SCHEDULER_LAYERED:
            switch (decoder->code) {
                case LDPC_DVBS2_DECODER_CODE_DVB_S2_64800_32400:
                    LDPC_DVBS2_DECODER_layered_64800x32400_dvb_s2_Terminate(decoder);
                    break;
                case LDPC_DVBS2_DECODER_CODE_DVB_S2_64800_6480:
                    LDPC_DVBS2_DECODER_layered_64800x6480_dvb_s2_Terminate(decoder);
                    break;
                case LDPC_DVBS2_DECODER_CODE_DVB_S2_64800_7200:
                    LDPC_DVBS2_DECODER_layered_64800x7200_dvb_s2_Terminate(decoder);
                    break;
                default:
                    ret = LDPC_DVBS2_DECODER_ERROR_RUNTIME;
                    fprintf(stderr, "Unkown code: %d\n", decoder->code);
                    break;
            }
            break;
        default:
            ret = LDPC_DVBS2_DECODER_ERROR_RUNTIME;
            fprintf(stderr, "Unkown scheduler: %d\n", decoder->scheduler);
            break;
    }

    return ret;
}

LDPC_DVBS2_DECODER_Error_t LDPC_DVBS2_DECODER_Decode(LDPC_DVBS2_DECODER_Decoder_t *decoder,
                            char input[],
                            char output[],
                            int iterationCount)
{
    LDPC_DVBS2_DECODER_Error_t ret = LDPC_DVBS2_DECODER_ERROR_NONE;

    switch(decoder->scheduler) {
        case LDPC_DVBS2_DECODER_SCHEDULER_FLOODED:
            switch (decoder->code) {
                case LDPC_DVBS2_DECODER_CODE_DVB_S2_64800_32400:
                    LDPC_DVBS2_DECODER_flooded_64800x32400_dvb_s2_Decode(decoder, input, output, iterationCount);
                    break;
                case LDPC_DVBS2_DECODER_CODE_DVB_S2_64800_6480:
                    LDPC_DVBS2_DECODER_flooded_64800x6480_dvb_s2_Decode(decoder, input, output, iterationCount);
                    break;
                case LDPC_DVBS2_DECODER_CODE_DVB_S2_64800_7200:
                    LDPC_DVBS2_DECODER_flooded_64800x7200_dvb_s2_Decode(decoder, input, output, iterationCount);
                    break;
                default:
                    ret = LDPC_DVBS2_DECODER_ERROR_RUNTIME;
                    fprintf(stderr, "Unkown code: %d\n", decoder->code);
                    break;
            }
            break;
    case LDPC_DVBS2_DECODER_SCHEDULER_LAYERED:
            switch (decoder->code) {
                case LDPC_DVBS2_DECODER_CODE_DVB_S2_64800_32400:
                    LDPC_DVBS2_DECODER_layered_64800x32400_dvb_s2_Decode(decoder, input, output, iterationCount);
                    break;
                case LDPC_DVBS2_DECODER_CODE_DVB_S2_64800_6480:
                    LDPC_DVBS2_DECODER_layered_64800x6480_dvb_s2_Decode(decoder, input, output, iterationCount);
                    break;
                case LDPC_DVBS2_DECODER_CODE_DVB_S2_64800_7200:
                    LDPC_DVBS2_DECODER_layered_64800x7200_dvb_s2_Decode(decoder, input, output, iterationCount);
                    break;
                default:
                    ret = LDPC_DVBS2_DECODER_ERROR_RUNTIME;
                    fprintf(stderr, "Unkown code: %d\n", decoder->code);
                    break;
            }
            break;
        default:
            ret = LDPC_DVBS2_DECODER_ERROR_RUNTIME;
            fprintf(stderr, "Unkown scheduler: %d\n", decoder->scheduler);
            break;
    }

    return ret;
}
